const { exec, execSync } = require('child_process');

console.log('. Installing required dependencies...');
execSync('yarn install', { cwd: __dirname });
console.log('. Finished installing required dependencies');

const inquirer = require('inquirer');
const colors = require('ansi-colors');
const ui = new inquirer.ui.BottomBar();

let orgsLoaded = false;
const orgList = [];
const appirio = { sonarQubeUrl: "https://sonarqube.appirio.com" };

const fetchAllOrgs = async () => {
  exec('sfdx force:org:list --json', (err, out) => {
    if (err) {
      throw err;
    }
    const orgJSON = JSON.parse(out).result;
    orgJSON.nonScratchOrgs.forEach((org) => {
      if (org.connectedStatus.toLowerCase() === 'connected') {
        const alias = org.alias || org.username;
        orgList.push({
          name: alias,
          value: org.orgId,
        });
      }
    });
    orgsLoaded = true;
  });
};

const checkOrgLoading = () => {
  return new Promise((resolve) => {
    const loader = ['/', '|', '\\', '-'];
    let i = 4;
    let timeout;
    const foo = () => {
      clearTimeout(timeout);
      if (!orgsLoaded) {
        ui.updateBottomBar('\n  ' + colors.yellow(loader[i++ % 4] + ' Still working to retrieve list of your authorized orgs...\n\n'));
        timeout = setTimeout(foo, 200);
      } else {
        ui.updateBottomBar('');
        ui.writeLog('\n  . ' + colors.green('Org list retrieved successfully!\n'));
        resolve();
      }
    };
    foo();
  });
};

module.exports = {
  source: { "**/config/*": true },
  ingredients: [{
    type: "list",
    name: "sandboxModel",
    message: "What is your dev environment strategy?",
    choices: [{
      name: "Shared / Non-Source Tracking Sandboxes",
      value: "shared",
    }, {
      name: "Source Tracking Sandboxes",
      value: "source-tracking",
    }],
    default: "shared",
  }, {
    // This is conditional question based on sandboxModel value.
    "type": "input",
    "name": "haveAuthorizedOrgs",
    "message": "Have you authorized the orgs you intend to use in this project? (Yes/No)",
    "default": "No",
    when: answers => (answers.sandboxModel !== 'shared'),
    validate: function (answer) {
      if (answer.trim().toUpperCase() !== 'YES') {
        return 'You should open a new terminal window to authorize the orgs, before you can proceed here.';
      }
      ui.writeLog('\n  . ' + colors.yellow('Initializing process to retrieve list of your authorized orgs...\n'));
      fetchAllOrgs()
        .catch(err => {
          throw err;
        });
      return true;
    },
  }, {
    type: "list",
    name: "enableSonarQube",
    message: "Enable quality scanning using SonarQube?",
    choices: ["Yes", "No"],
    filter: val => (val === "Yes"),
  }, {
    type: "input",
    name: "sonarUrl",
    message: "What's the URL of your SonarQube instance?",
    when: answers => answers.enableSonarQube,
    default: appirio.sonarQubeUrl,
  }, {
    // This is conditional question based on sandboxModel value.
    type: "list",
    name: "productionOrgId",
    message: "Which production org will be used to create sandboxes for this project?",
    when: answers => {
      if (answers.sandboxModel === 'shared') {
        answers.productionOrgId = '';
        return false;
      }
      return true;
    },
    choices: async function () {
      await checkOrgLoading();
      return orgList;
    },
    validate: function (answer) {
      if (answer == '') {
        return 'You must provide the production org name';
      }
      return true;
    },
  }],
};
